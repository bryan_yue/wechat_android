package com.example.model_wechat.enums

enum class Scene(val type: Int) {
    Session(0),
    Timeline(1),
    Favorite(2),
    SpecifiedContact(3)
}