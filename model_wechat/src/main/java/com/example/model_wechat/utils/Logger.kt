package com.example.model_wechat.utils

import android.util.Log
import com.example.model_wechat.WeChatHelper

object Logger {

    fun d(log: String?) {
        if (WeChatHelper.IS_LOGGABLE) {
            Log.d("Logger", log+"")
        }
    }
}