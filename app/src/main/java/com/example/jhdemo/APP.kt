package com.example.jhdemo

import android.app.Application
import com.example.model_wechat.WeChatHelper

class APP :Application() {


    override fun onCreate() {
        super.onCreate()

        WeChatHelper.getInstance(this).init(BuildConfig.DEBUG)
    }
}