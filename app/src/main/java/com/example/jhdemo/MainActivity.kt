package com.example.jhdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.model_wechat.WeChatHelper
import com.example.model_wechat.listener.OnWeChatAuthLoginListener
import com.example.model_wechat.net.response.AccessTokenInfo
import com.example.model_wechat.net.response.WeChatUserInfo
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() ,OnWeChatAuthLoginListener{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btn_wechat_login.setOnClickListener {
            if (WeChatHelper.getInstance(applicationContext).isWeChatAppInstalled(applicationContext)){
                //已安装微信
                WeChatHelper.getInstance(applicationContext).authLogin(this)
            }else{
                //
                runOnUiThread {
                    Toast.makeText(applicationContext, "微信未安装", Toast.LENGTH_SHORT).show()

                }
            }
        }
    }

    override fun onWeChatAuthLoginStart() {
        Log.e("WeChat","开始申请授权登录")
    }

    override fun onWeChatAuthLoginSuccess(
        accessTokenInfo: AccessTokenInfo?,
        weChatUserInfo: WeChatUserInfo?,
    ) {
        Log.e("WeChat","授权登录成功\n")
        Log.e("WeChat","weChatUserInfo${weChatUserInfo?.toString()}\n")
        Log.e("WeChat","accessTokenInfo${accessTokenInfo?.toString()}\n")
        tv_cont.post {
            tv_cont.text="${accessTokenInfo?.toformat()}"+"${weChatUserInfo?.toformat()}"
        }
    }

    override fun onWeChatAuthLoginCancel() {
        Log.e("WeChat","取消授权登录")
    }

    override fun onWeChatAuthLoginAuthDenied() {
        Log.e("WeChat","拒绝授权登录")
    }

    override fun onWeChatAuthLoginError(errorCode: Int?, errorMessage: String?) {
        Log.e("WeChat","授权登录异常 错误码:$errorCode,错误信息:$errorMessage")
    }
}